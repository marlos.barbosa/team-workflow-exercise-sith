import starWarsConsumer from "../consumer/starWarsConsumer.js";
import starWarsParser from "./starWarsParser.js";

// STAR WARS VIEW

// IMPORTANT: IMPLEMENT A CATCH BLOCK FOR EVERY CONSUMER FUNCTION

// TASK: Get planets data from starWarsConsumer and parse them to get the name of each planet with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewPlanetsName() {
  const toObtainPlanetsData = await starWarsConsumer.getPlanets()
  const planetsNames = starWarsParser.getPlanetsNames(toObtainPlanetsData)
  console.log('Nomes dos planetas: ', planetsNames)
}

// TASK: Get planets data from starWarsConsumer and parse them to get the rotation period of each planet with starWarsParser, then show the result in the console. TODO: **DEV 2**
async function viewPlanetsRotationPeriod() {
  const starWarsPlanet =  await starWarsConsumer.getPlanets();
  const rotationPlanets = await starWarsParser.getPlanetsRotationPeriod(starWarsPlanet);
  console.log('Rotações dos planetas: ', rotationPlanets);
}

// TASK: Get single planet data from starWarsConsumer and parse it to get its terrain with starWarsParser, then show the result in the console. TODO: **DEV 3**
async function viewSinglePlanetTerrain(planetId) {
  const toObtainPlanetTerrainData = await starWarsConsumer.getSinglePlanet(planetId)
  const singlePlanetTerrain = await starWarsParser.getSinglePlanetTerrain(toObtainPlanetTerrainData)
  console.log(`Terreno do planeta ${planetId}: `, singlePlanetTerrain)
}

// TASK: Get people data from starWarsConsumer and parse them to get the name of each person with starWarsParser, then show the result in the console. TODO: **DEV 4**
async function viewPeopleNames() {
  const gettingPeopleData = await starWarsConsumer.getPeople()
  const gettingPeopleNames = await starWarsParser.getPeopleNames(gettingPeopleData)
  console.log ('Nomes dos personagens: ', gettingPeopleNames)
}

// TASK: Get people data from starWarsConsumer and parse them to get the height of each person with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewPeopleHeights() {
  const toObtainPeopleData = await starWarsConsumer.getPeople()
  const peopleHeights = starWarsParser.getPeopleHeights(toObtainPeopleData)
  console.log('Alturas das pessoas: ', peopleHeights)
}

// TASK: Get single person data from starWarsConsumer and parse it to get its hair color with starWarsParser, then show the result in the console. TODO: **DEV 2**
async function viewSinglePersonHairColor(personId) {
  const singlePerson = await starWarsConsumer.getSinglePerson(personId);
  const personHairColor = await starWarsParser.getSinglePersonHairColor(singlePerson);
  console.log(`Cabelo da pessoa ${personId}: `, personHairColor);
}

// TASK: Get films data from starWarsConsumer and parse them to get the title of each film with starWarsParser, then show the result in the console. TODO: **DEV 3**
 async function viewFilmsTitles() {
  const toObtainFilmesTitlesData = await starWarsConsumer.getFilms()
  const filmsTitles = await starWarsParser.getFilmsTitles(toObtainFilmesTitlesData)
  console.log ('Títulos dos filmes: ', filmsTitles)
}

// TASK: Get single film data from starWarsConsumer and parse it to get its opening crawl with starWarsParser, then show the result in the console. TODO: **DEV 4**
async function viewSingleFilmOpeningCrawl(filmId) {
  const gettingSingleFilmData = await starWarsConsumer.getSingleFilm(filmId)
  const gettingSingleFilmOpeningCrawl = await starWarsParser.getSingleFilmOpeningCrawl(gettingSingleFilmData)
  console.log (`Introdução do filme ${filmId}: `, gettingSingleFilmOpeningCrawl)
}

// TASK: Get species data from starWarsConsumer and parse them to get the name of each species with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewSpeciesNames() {
  const toObtainSpeciesData = await starWarsConsumer.getSpecies()
  const speciesNames = starWarsParser.getSpeciesNames(toObtainSpeciesData)
  console.log('Nomes das espécies: ', speciesNames)
}

// TASK: Get single species data from starWarsConsumer and parse it to get its language with starWarsParser, then show the result in the console. TODO: **DEV 2**

async function viewSingleSpeciesLanguage(speciesId) {
  const starWarsSingleSpecies = await starWarsConsumer.getSingleSpecies(speciesId);
  const singleSpeciesLanguage = await starWarsParser.getSingleSpeciesLanguage(starWarsSingleSpecies);
  console.log(`Linguagem da espécie ${speciesId}: `, singleSpeciesLanguage);
}

// TASK: Get vehicles data from starWarsConsumer and parse them to get the name of each vehicle with starWarsParser, then show the result in the console. TODO: **DEV 3**
async function viewVehiclesNames() {
  const toObtainVehiclesNames = await starWarsConsumer.getVehicles()
  const vehiclesNames = await starWarsParser.getVehiclesNames(toObtainVehiclesNames)
  console.log('Nomes dos veículos: ', vehiclesNames)
}

// TASK: Get single vehicle data from starWarsConsumer and parse it to get its model with starWarsParser, then show the result in the console. TODO: **DEV 4**
async function viewSingleVehicleModel(vehicleId) {
  const gettingVehiclesNamesData = await starWarsConsumer.getSingleVehicle(vehicleId)
  const gettingSingleVehicleModel = await starWarsParser.getSingleVehicleModel(gettingVehiclesNamesData)
  console.log(`Modelo do veículo ${vehicleId}: `, gettingSingleVehicleModel)
}

// TASK: Get starships data from starWarsConsumer and parse them to get the name of each starship with starWarsParser, then show the result in the console. TODO: **DEV 1**
async function viewStarshipsNames() {
  const toObtainStarshipsData = await starWarsConsumer.getStarships()
  const starshipsNames = starWarsParser.getStarshipsNames(toObtainStarshipsData)
  console.log('Nomes das espaçonaves: ', starshipsNames)
}

// TASK: Get single starship data from starWarsConsumer and parse it to get its manufacturer with starWarsParser, then show the result in the console. TODO: **DEV 2**
async function viewSingleStarshipManufacturer(starshipId) {
  const starWarsSingleStarship = await starWarsConsumer.getSingleStarship(starshipId);
  const singleStarshipManufacture = await  starWarsParser.getSingleStarshipManufacturer(starWarsSingleStarship);
  console.log(`Fabricante da espaçonave ${starshipId}: `, singleStarshipManufacture);
}

const starWarsView = {
  viewPlanetsName,
  viewPlanetsRotationPeriod,
  viewSinglePlanetTerrain,
  viewPeopleNames,
  viewPeopleHeights,
  viewSinglePersonHairColor,
  viewFilmsTitles,
  viewSingleFilmOpeningCrawl,
  viewSpeciesNames,
  viewSingleSpeciesLanguage,
  viewVehiclesNames,
  viewSingleVehicleModel,
  viewStarshipsNames,
  viewSingleStarshipManufacturer,
};

export default starWarsView;
