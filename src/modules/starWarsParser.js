// LIST OF PARSER FUNCTIONS TO GET SPECIFIC DATA FROM THE STAR WARS API CONSUMER.

// TASK: Parse the star wars planets data to get the name of each planet. TODO: **DEV 1**
function getPlanetsNames(planets) {
  return planets.map(data => data.name)
}

// TASK: Parse the star wars planets data to get the rotation period of each planet. TODO: **DEV 2**
function getPlanetsRotationPeriod(planets) {
  let rotationPlanets = planets.map(a=> a.rotation_period);
  return rotationPlanets;
}

// TASK: Parse the star wars single planet data to get its terrain. TODO: **DEV 3**
function getSinglePlanetTerrain(planet) {
  const specificPlanetTerrain = planet.terrain
  return specificPlanetTerrain
}

// TASK: Parse the star wars people data to get the name of each person. TODO: **DEV 4**
function getPeopleNames(people) {
  let peopleNames = people.map(a=>a.name)
  return peopleNames
}

// TASK: Parse the star wars people data to get the height of each person. TODO: **DEV 1**
function getPeopleHeights(people) {
  return people.map(data => data.height)
}

// TASK: Parse the star wars single person data to get its hair color. TODO: **DEV 2**
function getSinglePersonHairColor(person) {
  const personHairColor = person.hair_color;
  return personHairColor;
}

// TASK: Parse the star wars films data to get the title of each film. TODO: **DEV 3**
function getFilmsTitles(films) {
  let filmTitles = films.map(a=>a.title)
  return filmTitles
}

// TASK: Parse the star wars single film data to get its opening crawl. TODO: **DEV 4**
function getSingleFilmOpeningCrawl(film) {
  const singleFilmOpeningCrawl = film.opening_crawl
  return singleFilmOpeningCrawl
}

// TASK: Parse the star wars species data to get the name of each species. TODO: **DEV 1**
function getSpeciesNames(species) {
  return species.map(data => data.name)
}

// TASK: Parse the star wars single species data to get its language. TODO: **DEV 2**
function getSingleSpeciesLanguage(specie) {
  const singleSpeciesLanguage = specie.language;
  return singleSpeciesLanguage; 
}

// TASK: Parse the star wars vehicles data to get the name of each vehicle. TODO: **DEV 3**
function getVehiclesNames(vehicles) {
  const vehiclesNames = vehicles.map(a=>a.name)
  return vehiclesNames
}

// TASK: Parse the star wars single vehicle data to get its model. TODO: **DEV 4**
function getSingleVehicleModel(vehicle) {
  const singleVehicleModel = vehicle.model
  return singleVehicleModel
}

// TASK: Parse the star wars starships data to get the name of each starship. TODO: **DEV 1**
function getStarshipsNames(starships) {
  return starships.map(data => data.name)
}

// TASK: Parse the star wars single starship data to get its manufacturer. TODO: **DEV 2**
function getSingleStarshipManufacturer(starship) {
  const singleStarshipManufacture = starship.manufacturer;
  return singleStarshipManufacture;
}

const starWarsParser = {
  getPlanetsNames,
  getPlanetsRotationPeriod,
  getSinglePlanetTerrain,
  getPeopleNames,
  getPeopleHeights,
  getSinglePersonHairColor,
  getFilmsTitles,
  getSingleFilmOpeningCrawl,
  getSpeciesNames,
  getSingleSpeciesLanguage,
  getVehiclesNames,
  getSingleVehicleModel,
  getStarshipsNames,
  getSingleStarshipManufacturer,
};

export default starWarsParser;
