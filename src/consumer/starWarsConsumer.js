 import axios from "axios";

// STAR WARS API CONSUMPTION

// TASK: Consume star wars API to get planets data. - TODO: **DEV 1**
async function getPlanets() {
  const response = await axios.get("https://swapi.dev/api/planets/")
  const starWarsPlanets = response.data.results
  return starWarsPlanets
}

// TASK: Consume star wars API to get people data. - TODO: **DEV 2**
async function getPeople() {
  const response = await axios.get('https://swapi.dev/api/people/');
  const starWarsPeople=response.data.results;
    return starWarsPeople;
}

// TASK: Consume star wars API to get films data. - TODO: **DEV 3**
async function getFilms() {
  const response = await axios.get("https://swapi.dev/api/films/")
  const starWarsFilms = response.data.results
  return starWarsFilms
}

// TASK: Consume star wars API to get species data. - TODO: **DEV 4**
async function getSpecies() {
  const response = await axios.get("https://swapi.dev/api/species/")
  const starWarsSpecies = response.data.results
  return starWarsSpecies
}

// TASK: Consume star wars API to get vehicles data. - TODO: **DEV 1**
async function getVehicles() {
  const response = await axios.get("https://swapi.dev/api/vehicles/")
  const starWarsVehicles = response.data.results
  return starWarsVehicles
}

// TASK: Consume star wars API to get starships data. - TODO: **DEV 2**
async function getStarships() {
  const response = await axios.get('https://swapi.dev/api/starships/');
  const starWarsStarships=response.data.results;
  return starWarsStarships;
}

// TASK: Consume star wars API to get a single planet data. - TODO: **DEV 3**
async function getSinglePlanet(id) {
   const response = await axios.get(`https://swapi.dev/api/planets/${id}/`)
   const starWarsSinglePlanet = response.data
   return starWarsSinglePlanet
 }


// TASK: Consume star wars API to get a single person data. - TODO: **DEV 4**
async function getSinglePerson(id) {
  const response = await axios.get(`https://swapi.dev/api/people/${id}`)
  const starWarsPerson = response.data
  return starWarsPerson
}

// TASK: Consume star wars API to get a single film data. - TODO: **DEV 1**
async function getSingleFilm(id) {
  const response = await axios.get(`https://swapi.dev/api/films/${id}/`)
  const starWarsSingleFilm = response.data
  return starWarsSingleFilm
}

// TASK:  Consume star wars API to get a single species data. - TODO: **DEV 2**
async function getSingleSpecies(id) {
  const response = await axios.get(`https://swapi.dev/api/species/${id}/`);
  const starWarsSingleSpecies = response.data;
  return starWarsSingleSpecies;
}

// TASK: Consume star wars API to get a single vehicle data. - TODO: **DEV 3**
async function getSingleVehicle(id) {
  const response = await axios.get(`https://swapi.dev/api/vehicles/${id}/`)
  const starWarsSingleVehicle = response.data
  return starWarsSingleVehicle
}

// TASK: Consume star wars API to get a single starship data. - TODO: **DEV 4**
async function getSingleStarship(id) {
  const response = await axios.get(`https://swapi.dev/api/starships/${id}`)
  const starWarsStarship = response.data
  return starWarsStarship
}

const starWarsConsumer = {
 getPlanets,
 getPeople,
 getFilms,
 getSpecies,
getVehicles,
 getStarships,
 getSinglePlanet,
 getSinglePerson,
 getSingleFilm,
 getSingleSpecies,
 getSingleVehicle,
 getSingleStarship,
};

export default starWarsConsumer;
